from django.test import TestCase
from django.utils.timezone import utc
from django.core.exceptions import ValidationError
from backend import ops
from backend import const
from backend.unittest import PersonFixtureMixin
import datetime

class TestPerson(PersonFixtureMixin, TestCase):
    def test_uid(self):
        self.persons.dc.uid = "foo"
        self.persons.dc.full_clean()
        self.persons.dc.save(audit_skip=True)
        self.persons.dc.uid = "foo-guest"
        with self.assertRaises(ValidationError):
            self.persons.dc.full_clean()

