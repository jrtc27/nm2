{% load nm %}

Dear debian-private,

This is a last attempt to make contact with {{person.fullname}} (CC'd),
or find out if anyone in the project is able to provide us with further
details about their whereabouts. If you think you can help please let us
know by replying to this email or, for a more confidential channel,
replace debian-private with wat@debian.org.

You can follow the tracking of this process on nm.debian.org[0].

The Missing In Action team has been trying to reach out to this
developer for some time; more details can be seen via mia-query[1].
Unfortunately this was not successful and a final attempt was made via
the nm.debian.org system on {{process.started|date:"F d, Y"}}.

In the absence of a better alternative, we will request that
keyring-maint remove any active key and DSA shut down this developer's
accounts after {{deadline|date:"F d, Y"}}.

For the Front Desk / MIA team,
{{visitor.fullname}}

[0] {{process_url}}
[1] ssh qa.debian.org /srv/qa.debian.org/mia/mia-query {{person.uid}}
